/*import feathers from 'feathers/client'
import rest from 'feathers-rest/client'*/

import feathers from '@feathersjs/feathers'
import rest from '@feathersjs/rest-client'

/*const feathers = require('@feathersjs/feathers');
const rest = require('@feathersjs/rest-client');*/

/*console.log(feathers)
console.log(rest)*/

const client = feathers().configure(rest('http://localhost:3030').fetch(window.fetch.bind(window)))

export default client;
