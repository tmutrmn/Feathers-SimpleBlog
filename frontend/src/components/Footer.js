import React, { Component } from 'react';

class Footer extends Component {
  render() {
		return (
			<footer className="footer">
				<div className="container">
					<div className="content has-text-centered">
						<p>
							<strong>SimpleBlog</strong> by <a href="https://gitlab.com/tmutrmn">tmutrmn</a>.
							<br /><br />
							this project is made with: <br /><br />
							<a>skillz</a>
							<br />
							<a href="https://es6.io">ES6</a>
							<br />
							<a href="https://feathersjs.com">FeatherJS</a>
							<br />
							<a href="https://reactjs.org">React</a>
							<br />
							<a href="https://bulma.io">Bulma</a>
							<br /><br />
							copyleft &copy; tmutrmn 2017
						</p>
					</div>
				</div>
			</footer>
		);
	}
}

export default Footer;
