import React from "react";
import CommentForm from "./CommentForm";

import client from '../feathers';



class CommentList extends React.Component {

	constructor(props) {
		super(props);
		console.log(props)
		this.state = {
			postId: this.props.id,
			comments:[]
		};
		this.handleCommentSubmit = this.handleCommentSubmit.bind(this);
  }


	componentWillMount() {
		let _this = this;
		const comments = client.service('/posts/' + this.state.postId + '/comments');			// comments service

		comments.find().then(results => {
			console.log(results)
			_this.setState({
				comments: results.data
			})
		}).catch((error) => {
			console.log(error)
		})
  }


  componentDidMount() {
		// never run setState here because it will trigger an endless update loop...
	}


	handleCommentSubmit(comment) {
		let _comments = this.state.comments
		console.log(comment);

		const headers = new Headers({ 'Accept': 'application/json', 'Content-Type': 'application/json', 'cache-control': 'no-cache' });
		const conf = {
			method: 'POST',
			headers: headers,	// https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch#Headers
			mode: 'cors',		// same-origin, no-cors, cors - use cors!	(when the options mode property is set to no-cors the request header values are immutable.)
			credentials: 'omit',		// omit, same-origin, include		use omit...
			body: JSON.stringify(comment)
		};
		const apiRequest = new Request('http://localhost:3030/posts/' + comment.postId + '/comments', conf);

		fetch(apiRequest).then((response) => {
			let contentType = response.headers.get("content-type")
			if(contentType && contentType.includes("application/json")) {
				return response.json()
			}
			throw new TypeError("Error, returned data was not JSON!")
		})
		.then((json) => {
			console.log(json)
			comment.id = json._id;		// ass we do
			_comments.push(comment);
			this.setState({
				comments: _comments
			})
		})
		.catch((error) => {
			console.log(error)
		})
	}

	render() {
		/*return (
			<div>
				<h3>comments:</h3>
				<ul>
					{ this.state.comments.length ?
						this.state.comments.map(comment=><li key={comment._id}><hr /><p><strong>{comment.name}</strong> said on {comment.date}: </p><p>{comment.text}</p></li>)
						: <li>No comments. Be first to comment...</li> }
				</ul>
				<CommentForm key='comment-form' id={this.props.id} onCommentSubmit={this.handleCommentSubmit} />
			</div>
		)*/
		return (
			<div>
				<h4 className="subtitle is-4">Comments:</h4>
				{ this.state.comments.length ? this.state.comments.map(comment=>
					<article className="media" key={comment._id}>
						<figure className="media-left">
							<p className="image is-48x48">
								<img src="https://bulma.io/images/placeholders/96x96.png" />
							</p>
						</figure>
						<div className="media-content">
							<div className="content">
								<p>
									<strong>{comment.name}</strong>
									<br />
									{comment.text}
									<br />
									<small><a>Like</a> · <a>Reply</a> · {comment.date}</small>
								</p>
							</div>
						</div>
					</article>)
					:
					<article className="message">
						<div className="message-body">
							No comments. Be first to comment...
						</div>
					</article> }

					<CommentForm key='comment-form' id={this.props.id} onCommentSubmit={this.handleCommentSubmit} />
			</div>
		)
  }
}

export default CommentList;
