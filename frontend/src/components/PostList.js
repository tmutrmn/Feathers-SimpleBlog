import React from "react";
import { Link } from 'react-router-dom';
import PostForm from "./PostForm";

import client from '../feathers';
const posts = client.service('/posts');			// posts service


class PostListItem extends React.Component {
	/* render() {
		return (
			<li>
				<Link to={'/posts/' + this.props.id}>{this.props.title}</Link>
			</li>
		)
	} */

	shorten(text, maxLength) {
		if (text.length > maxLength) return	text = text.substr(0, maxLength - 3) + "...";
		else return text;
	}

	render() {
		return (
			<div className="box">
				<article className="media">
					<div className="media-left">
						<figure className="image is-64x64">
							<img src="https://bulma.io/images/placeholders/128x128.png" alt="Image" />
						</figure>
					</div>
					<div className="media-content">
						<div className="content">
								<strong>{this.props.name}</strong> <small>@{this.props.name}</small> <small>{this.props.pubDate}</small>
								<br />
								<Link to={'/posts/' + this.props.id}>
									<h3 className="subtitle is-3">{this.props.title}</h3>
									<p className="post-list">{this.shorten(this.props.content, 32)}</p>
								</Link>
								<Link to={'/posts/' + this.props.id} className="button"> Read more </Link>
						</div>
					</div>
				</article>
			</div>
		)
	}
}


class PostList extends React.Component {
	constructor() {
		super();
		this.state = { items:[] };
		this.handlePostSubmit = this.handlePostSubmit.bind(this);
  }

	componentWillMount() {
		let _this = this;

		posts.find().then(results => {
			console.log(results.data)
			_this.setState({
				items: results.data
			})
		}).catch((error) => {
			console.log(error)
		})
  }

  componentDidMount() {
		// never run setState here because it will trigger an endless update loop...
	}


	handlePostSubmit(post) {
		let _this = this;
		let _posts = this.state.items;

		posts.create(post).then(results => {
			post._id = results._id;		// set the id
			_posts.push(post);
			_this.setState({
				items: _posts
			})
		}).catch((error) => {
			console.log(error)
		})
	}


	render() {
		return(
			<div>
				{ this.state.items.length ? this.state.items.map(item => <PostListItem key={item._id} id={item._id} name={item.name} title={item.title} content={item.content} pubDate={item.pubDate} />) : <li>Loading...</li> }
				<a name='postform'></a>
				<PostForm key='post-form' onPostSubmit={this.handlePostSubmit} />
			</div>
		)
  }
}

export default PostList;
