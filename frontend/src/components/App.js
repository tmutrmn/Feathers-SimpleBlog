import React, { Component } from 'react';
import PostList from "./PostList";
import Footer from "./Footer";
import 'bulma/css/bulma.css'
import '../App.css';

// const serverUrl = 'http://localhost:3030';

class App extends Component {
  render() {
		return (
			<div className="container is-fluid">
				<a name='pagetop'></a>
				<section className="hero is-primary is-medium is-bold">
					<div className="hero-body">
						<div className="container">
							<h1 className="title is-1">SIMPLE BLOG</h1>
							<h3 className="subtitle is-4">Simple Blog app done with FeathersJS-backend and React/Feathers frontend</h3>
						</div>
					</div>
				</section>
				<br />
				<a href="#postform" className='button is-outlined'> Add New Blog Post </a>
				<br /><br />
				<PostList />
				<br />
				<a href="#pagetop" className='button is-outlined'> Back to Top </a>
				<br /><br />
				<Footer />
			</div>
    );
  }
}

export default App;
