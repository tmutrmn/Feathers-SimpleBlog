import React from "react";


class PostForm extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			name: '',
			title: '',
			content: ''
		};

		this.onChange = this.onChange.bind(this);
		/*this.onNameChange = this.onNameChange.bind(this);
		this.onTitleChange = this.onTitleChange.bind(this);
		this.onContentChange = this.onContentChange.bind(this);*/
		this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
		// let _this = this;
	}


	onChange(event) {
		this.setState({ [event.target.name]: event.target.value })
	}

	handleSubmit(event) {
		event.preventDefault();
		console.log("Submitted values are: ", event.target.title.value, event.target.content.value);

		let title = this.state.title;
    let content = this.state.content.trim();

    this.props.onPostSubmit({ title: title, content: content, pubDate: new Date().toISOString() });		// bubble up to parent

    this.setState({		// clear state & form
      title: '',
      content: ''
    });
	}

	render() {
		return (
			<div className="box">
				<article className="media">
					<div className="media-content">
						<div className="content">
							<h3 className="subtitle is-3">Post a new post</h3>

							<form onSubmit={this.handleSubmit}>

								<div className="field">
									<label className="label">Name</label>
									<div className="control">
										<input className="input size-20"
											type="text"
											name='name'
											placeholder="Your name"
											value={this.state.name}
											onChange={this.onChange} />
									</div>
								</div>

								<div className="field">
									<label className="label">Title</label>
									<div className="control">
										<input className="input size-20"
											type='text'
											name='title'
											placeholder='Post title'
											value={this.state.title}
											onChange={this.onChange} />
									</div>
								</div>

								<div className="field">
									<label className="label">Message</label>
									<div className="control">
										<textarea className="textarea"
											name='content'
											placeholder="Post content here..."
											value={this.state.content}
											onChange={this.onChange} />
									</div>
								</div>

								<div className="field">
									<div className="control">
										<label className="checkbox">
											<input type="checkbox" />
											&nbsp; I agree to the <a href="#">terms and conditions</a>
										</label>
									</div>
								</div>

								<button className="button" type="submit">&nbsp; Post post &nbsp;</button>
							</form>

						</div>
					</div>
				</article>
			</div>
		)
  }
}

export default PostForm;
