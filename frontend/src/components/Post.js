import React from "react";
import { Link } from "react-router-dom";
import CommentList from "./CommentList";

import client from '../feathers';
const posts = client.service('/posts');			// posts service


class Post extends React.Component {
	constructor(props) {
		super(props);
		// console.log(this.props.routeParams.id);
		this.state = {
			id: this.props.match.params.id,		// this.props.routeParams.id,
			data: {}
    }
  }

	componentWillMount() {
		let _this = this;

		posts.get(this.state.id).then(results => {
			// console.log(results)
			_this.setState({
				data: results
			})
		}).catch((error) => {
			console.log(error)
		})
  }

  componentDidMount() {
		// never run setState here because it will trigger an endless update loop...
	}


	render() {		// shouild use moment.js for formatting date data...
		return(
			<div className="container is-fluid">
				<section className="hero is-primary is-bold">
				<div className="hero-body">
					<div className="container is-fluid">
						<h6 className="subtitle is-6">{this.state.data.pubDate}</h6>
						<h1 className="title is-1">{this.state.data.title}</h1>
						<h4 className="subtitle is-4">by {this.state.data.name}</h4>
					</div>
				</div>
				</section>
				<div className="box">
					<article className="media">
						<div className="media-content">
							<div className="content">
								<p className="is-size-4">{this.state.data.content}</p>
								<CommentList key={this.state.id} id={this.state.id} />
								<br />
								<Link to='/' className="button is-primary is-outlined">&laquo; Back &nbsp;</Link>
							</div>
						</div>
					</article>
				</div>
			</div>
		)
  }
}

export default Post;
