import React from "react";


class CommentForm extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			name: '',
			comment: ''
		};

		this.onNameChange = this.onNameChange.bind(this);
		this.onCommentChange = this.onCommentChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
		// let _this = this;
  }

	onNameChange(event) {
		this.setState({
			name: event.target.value
		});
	}

	onCommentChange(event) {
		this.setState({
			comment: event.target.value
		});
	}

	handleSubmit(event) {
		event.preventDefault();
		// console.log("Submitted values are: ", event.target.name.value, event.target.comment.value);

		let name = this.state.name;
    let comment = this.state.comment.trim();

    this.props.onCommentSubmit({ name: name, text: comment, postId: this.props.id, date: new Date().toISOString() });		// bubble up to parent

    this.setState({		// clear state & form
      name: '',
      comment: ''
    });
	}

	render() {
		return(

			<article className="media">
				<figure class="media-left">
					<p class="image is-64x64">
						<img src="https://bulma.io/images/placeholders/128x128.png" />
					</p>
				</figure>
				<div className="media-content">
					<div className="content">
						<h3 className="subtitle is-3">Post a new comment</h3>

			<form onSubmit={this.handleSubmit}>

				<div className="field">
					<label className="label">Name</label>
					<div className="control">
						<input className="input size-20" type="text"
							name='name'
							placeholder="Your name"
							value={this.state.name}
							onChange={this.onNameChange} />
					</div>
				</div>

				<div className="field">
					<label className="label">Comment</label>
					<div className="control">
						<textarea className="textarea"
							name='comment'
							placeholder="Your comment here..."
							value={this.state.comment}
							onChange={this.onCommentChange} />
					</div>
				</div>

				<button className="button" type="submit">Post comment</button>
			</form>

						</div>
					</div>
				</article>

		)
  }
}

export default CommentForm;
