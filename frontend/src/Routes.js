import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
// import { history } from "./store.js";
import App from "./components/App";
import PostList from "./components/PostList";
import Post from "./components/Post";
/*
import Albums from "./components/Albums";
import Album from "./components/Album";
import Image from "./components/Image";
import About from "./components/About";
*/
import NotFound from "./components/NotFound";


//
const Routes = (
  <BrowserRouter>
    <div>
      <Switch>
        <Route exact path="/" component={App} />
				<Route exact path="/posts" component={PostList} />
				<Route path="/posts/:id" component={Post} />
        <Route component={NotFound} />
      </Switch>
    </div>
  </BrowserRouter>
)

// export { Routes };
export default Routes;
