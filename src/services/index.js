const posts = require('./posts/posts.service.js');
const comments = require('./comments/comments.service.js');
module.exports = function (app) {

  app.configure(posts);
  app.configure(comments);

  /*server.service('/comments/:commentId/likes').before(hook =>
    hook.params.query.commentId = hook.params.commentId
  );*/

};
