// A hook that updates `data` with the route parameter
function mapPostIdToData(context) {
	if (context.data && context.params.postId) {
		context.data.postId = context.params.postId;
	}
}


module.exports = {
	/*
	before: {
    all: [
			hook => {
				console.log("postId: ", hook.params.postId);
			}],
    find: mapPostIdToData,
    get: mapPostIdToData,
    create: mapPostIdToData,
    update: mapPostIdToData,
		patch: mapPostIdToData,
		remove: mapPostIdToData,
  } ,
	*/
  before: {
		all: [
			(context) => {
				console.log('all:')
				console.log(context.params.query, context.params.route)
			}
		],
		find: [
			(context) => {		// find(context) {
				console.log('find:')
				console.log(context.params)
				context.params.query.postId = context.params.route.postId
				console.log(context.params)
			}
		],
    get: [
			(context) => {
				console.log('get:')
				console.log(context.params.query, context.params.route)
			}
		],
    create: [mapPostIdToData],
    update: [mapPostIdToData],
    patch: [mapPostIdToData],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
